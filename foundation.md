# Foundation Skills

* turn on a device
* use the available controls on my device
* make use of accessibility tools on my device to make it easier to use
* interact with the home screen on my device
* understand that the internet allows me to access information and content and that I can connect to it through Wi-Fi
* connect my device to a safe and secure Wi-Fi network
* connect to the internet and open a browser to find and use websites
* understand that my passwords and personal information need to be kept safely as they have value to others
* update and change my password when prompted to do so

# Digital foundation skills examples

I can:

* turn on the device and enter any account information as required
* use a mouse and keyboard on a computer, use a touch screen on a smart phone or tablet
* use settings menus to change device display to make content easier to read
* find applications by choosing the correct icons on the home screen
* connect a device to the internet using the Wi-Fi settings, and insert the password when required
* locate the browser icon on a device and find a website
* keep login information for a device and any websites secure, not shared with anyone or written down and left prominently near my device

The information presented above is done so under the conditions set out on the [source](./README.md) page

<hr>

## How we can do this with Free, Libre, and Open Source software and privacy friendly alternatives

* Password managers
  * [keepass](https://keepass.info/)
  * Thunderbird/Firefox/IceCat Password managers
    Both have built in password managers.

* Hardware and related (if applicable)
  * GNU/Linux phones
    * [Pinephone](https://wiki.pine64.org/wiki/PinePhone_Software_Releases)
    * [Librem 5](https://puri.sm/products/librem-5/)
