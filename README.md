# DigitalSkills

[The UK Government's Essential Digital Skills Framework](https://www.gov.uk/government/publications/essential-digital-skills-framework), improved to use free software!

## Credits/License

This is based on the UK Government's [Essential Digital Skills Framework](https://www.gov.uk/government/publications/essential-digital-skills-framework) – published 2019, a common framework designed to support providers, organisations and employers across the UK who offer training for adults to secure their essential digital skills,
which is licensed under the [Open Government License Version 3](https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/).
All changes to that are licensed under the [Creative Commons Attribution 4.0 license](https://creativecommons.org/licenses/by/4.0/).

## Compiling to HTML

You can compile this to HTML (into the output directory `html/`) using:

```sh
make html
```

You can remove it using:

```sh
make clean
```

or you can just browse the Markdown.

## Formatting guidelines

* No trailing whitespace
* Use 1 space in between sentences and after commas
* Use 1 space in between list markers and list content
* Use UNIX style newlines (line feeds)

To automatically format run:

```sh
make format
```
